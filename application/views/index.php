<div class="span4">
    <h3>
        Logga in
    </h3>
    <?php
        if(isset($login_errors)){
            $err_message = "";
            foreach($login_errors as $k => $v){
                if($err_message != ''){
                    $err_message .= '<br>';
                }
                $err_message .= $v;
            }
    ?>   
    <div>
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $err_message;?>
        </div>
    </div>
    <?php
        }
    ?>
    
    <form method="POST" action="/">
        <label>E-post</label>
        <input type="text" name="email" />
        <label>Lösenord</label>
        <input type="password" name="password" />
        <br />
        <input class="btn-success" type="submit" value="Logga in" />
    </form>
    <br />
    <a href="/welcome/signup">Inget konto? Registrera dig.</a>
</div>