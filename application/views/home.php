<div class="span4">
    <h3>
        Hej 
        <?php
            echo $this->session->userdata('user_email');
        ?>
    </h3>
    <p>
        Dina senaste inloggningar
    </p>
    <table class="table table-bordered">
        <tr>
            <td>
                <b>Datum</b>
            </td>
            <td>
                <b>Klockslag</b>
            </td>
        </tr>
        <?php
            for($i=0;$i<count($user_logins);$i++){
                $user_login = $user_logins[$i];
        ?>   
        <tr>
            <td>
                <?php 
                    echo htmlentities(substr($user_login->login_date, 0, 10));
                ?>
            </td>
            <td>
                <?php 
                    echo htmlentities(substr($user_login->login_date, 10, 9));
                ?>
            </td>
        </tr>
        <?php
            }
        ?>
    </table>
    <br />
    <form method="POST" action="/welcome/logout">
        <button type="submit" class="btn-danger">Logga ut</button>
    </form>
    
</div>