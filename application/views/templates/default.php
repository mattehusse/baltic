<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Baltic.com</title>

	<meta 
        name="keywords" 
        content="Baltic.com" />
    <meta 
        name="description" 
        content="Baltic.com">
    <script type="text/javascript" src="/bootstrap/js/jquery.js"></script>
    <link type="text/css" rel="stylesheet" href="/bootstrap/css/bootstrap.css" />
    <link type="text/css" rel="stylesheet" href="/bootstrap/css/style.css" />
    <link type="text/css" rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css" />
    <!--[if lt IE 9]>
            <link rel="stylesheet" type="text/css" href="/bootstrap/css/ie.css" />
    <![endif]-->
    
</head>
<body>
    <div class="wrapper">
    <header class="main">
        <div class="container">
            <h1>
                LOGO
            </h1>
            <hr />
        </div>
    </header>
    <div class="page-content">
        <div class="row-fluid">
            <div class="container">
                <?php echo $body;?>
            </div> <!-- /.container -->
        </div> <!-- /.row-fluid -->
    </div> <!-- /.page-content -->    
    <footer class="main">
        <div class="container">
            <div class="row-fluid">
                <hr />
                &copy; Alfredo Capasso
            </div>
	</div> 
    </footer>
<script type="text/javascript" src="/bootstrap/js/bootstrap.js"></script>
</body>
</html>