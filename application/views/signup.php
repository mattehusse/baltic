<div class="span4">
    <h3>
        Registrera
    </h3>
    <?php
        if(isset($signup_errors)){
            $err_message = "";
            foreach($signup_errors as $k => $v){
                if($err_message != ''){
                    $err_message .= '<br>';
                }
                $err_message .= $v;
            }
    ?>   
    <div>
        <div class="alert alert-error">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <?php echo $err_message;?>
        </div>
    </div>
    <?php
        }
    ?>
    <form method="POST" action="/welcome/signup">
        <label>E-post</label>
        <input type="text" name="email" value="<?php echo isset($set_values) ? htmlentities($set_values->email) : '';?>"/>
        <label>Lösenord</label>
        <input type="password" name="password" />
        <br />
        <input class="btn-success" type="submit" value="Registrera" />
    </form>
</div>