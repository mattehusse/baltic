<?php
class User_logins_model extends CI_Model {

    const DB_TABLE = "user_logins";
    var $id   = '';
    var $user_id = '';
    var $login_date    = '';
    
    function __construct(){
        parent::__construct();
    }
    
    
    function user_logins($user_id){
        $this->db->select("*");
        $this->db->from(self::DB_TABLE);
        $this->db->where('user_id', $user_id);
        $this->db->order_by('login_date', 'desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function create($user_id){
        $fields->login_date = date('Y-m-d H:i:s');
        $this->id					= "NULL";
	$this->user_id					= $user_id;
	$this->login_date				= $fields->login_date;
        $this->db->insert(self::DB_TABLE, $this);
    }
    
}