<?php
class User_model extends CI_Model {

    const DB_TABLE = "user";
    var $id   = '';
    var $email = '';
    var $password    = '';
    var $created_at    = '';
    
    function __construct(){
        parent::__construct();
    }
    
    
    function user_login($fields){
        $this->db->select("*");
        $this->db->from(self::DB_TABLE);
        $this->db->where('email', $fields->email);
        $this->db->where('password', md5($fields->password));
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    
    function email_exists($email, $id=''){
        $this->db->select("*");
        $this->db->from(self::DB_TABLE);
        $this->db->where('email', $email);
        if($id != ''){
            $this->db->where('id != ', $id);
        }
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result();
        return !empty($result);
    }
    
    function create($fields){
        $fields->created_at = date('Y-m-d H:i:s');
        $fields->password = md5($fields->password);
        $this->id					= "NULL";
	$this->email					= $fields->email;
        $this->password					= $fields->password;
	$this->created_at				= $fields->created_at;
        $this->db->insert(self::DB_TABLE, $this);
        $insert_id = $this->db->insert_id();
        if($this->db->_error_message() != ''){
            log_message('error', 'Mysql error: ' . $this->db->_error_message());
            log_message('error', 'Query error: ' . $this->db->last_query());
            return NULL;
        }
        
        return $insert_id;
    }
    
}