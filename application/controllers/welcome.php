<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {
    var $template_layout;
    
    function __construct(){
        parent::__construct();
        $this->load->library('template');
        $this->template_layout = "default";
        $this->load->model("User_model");
    }
    
    private function _validate_password($password){
        if(strlen($password)<6){
            return FALSE;
        }
        if(!preg_match('#[0-9]#',$password)){
            return FALSE;
        }
        if(!preg_match('/[A-Z]/', $password)){
            return FALSE;
        }
        return TRUE;
    }
    
    private function _validate_mail($email){
        if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)){
            return FALSE;
        }
        return TRUE;
    }
    
    public function signup(){
        $data = array();
        if($this->input->post()){
            $signup_errors = array();
            $fields = new stdClass();
            $fields->email = $this->input->post('email');
            $fields->password = $this->input->post('password');
            $validate_mail = $this->_validate_mail($fields->email);
            $validate_password = $this->_validate_password($fields->password);
            $email_exists = $this->User_model->email_exists($fields->email);
            if(!$validate_mail){
                $signup_errors[] = 'Ange giltig e-postadress';
            }
            if(!$validate_password){
                $signup_errors[] = 'Ange lösenord med minst en versal och en siffra, 6 tecken långt';
            }
            if($email_exists){
                $signup_errors[] = 'E-postadressen används redan';
            }
            if(count($signup_errors)){
                $fields->password = '';
                $data['set_values'] = $fields;
                $data['signup_errors'] = $signup_errors;
            }
            else{
                $user_id = $this->User_model->create($fields);
                if($user_id === NULL){
                    $signup_errors[] = 'Kontot kunde inte skapas';
                    $data['signup_errors'] = $signup_errors;
                }
                else{
                    header('Location: /welcome/thanks');
                    exit;
                }
            }
        }
        $this->template->load($this->template_layout, 'signup', $data);
    }
    
    public function thanks(){
        $data = array();
        $this->template->load($this->template_layout, 'thanks', $data);
    }
    
    public function home(){
        $user_id = $this->session->userdata('user_id');
        if($user_id == ''){
            header('Location: /');
            exit;
        }
        $data = array();
        $this->load->model("User_logins_model");
        $data['user_logins'] = $this->User_logins_model->user_logins($user_id);
        $this->template->load($this->template_layout, 'home', $data);
    }
    
    public function logout(){
        $this->session->unset_userdata('user_id');
        header('Location: /');
        exit;
    }

    public function index(){
        $data = array();
        if($this->input->post()){
            $login_errors = array();
            $fields = new stdClass();
            $fields->email = $this->input->post('email');
            $fields->password = $this->input->post('password');
            $user_login = $this->User_model->user_login($fields);
            if(empty($user_login)){
                $login_errors[] = 'Inloggning misslyckades';
                $data['login_errors'] = $login_errors;
            }
            else{
                $us = $user_login[0];
                $this->session->set_userdata('user_id',  $us->id);
                $this->session->set_userdata('user_email',  $us->email);
                $this->load->model("User_logins_model");
                $this->User_logins_model->create($us->id);
                header('Location: /welcome/home');
                exit;
            }
        }
        $this->template->load($this->template_layout, 'index', $data);
    }
}