drop table if exists user;
CREATE TABLE IF NOT EXISTS  user(
    id                                      INT(7)		NOT NULL AUTO_INCREMENT
    ,email                                  VARCHAR(80)		NOT NULL 
    ,password                               VARCHAR(80)		NOT NULL 
    ,created_at                             DATETIME		NOT NULL 
    ,PRIMARY KEY (id)
    ,UNIQUE (email)
    ,INDEX (password)
    ,INDEX (created_at)
) ENGINE = INNODB;

drop table if exists user_logins;
CREATE TABLE IF NOT EXISTS  user_logins(
    id                                      INT(9)		NOT NULL AUTO_INCREMENT
    ,user_id                                INT(7)		NOT NULL 
    ,login_date                             DATETIME		NOT NULL 
    ,PRIMARY KEY (id)
    ,INDEX (user_id)
    ,INDEX (login_date)
) ENGINE = INNODB;

